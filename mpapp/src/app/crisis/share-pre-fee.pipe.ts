import {Pipe, PipeTransform} from '@angular/core';
import {Authenticate} from '../core/authenticate.service';

@Pipe({
  name: 'sharePreFee'
})
export class SharePreFeePipe implements PipeTransform {

  constructor(private authenticate: Authenticate) {
  }

  transform(value: number): number {
    if (this.authenticate.isLoggedIn) {
      const free = value * this.authenticate.authenticator.wallet.rate;
      return Math.round(free * 100) / 100;
    }
    return 0;
  }

}
