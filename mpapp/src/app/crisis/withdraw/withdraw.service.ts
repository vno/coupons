import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Wallet} from './wallet';

@Injectable({
  providedIn: 'root'
})
export class WithdrawService {

  constructor(private http: HttpClient) {
  }

  withdraw(params: any): Observable<any> {
    return this.http.post(`/wx/withdraw`, params);
  }

  getWallet(openId: string): Observable<Wallet> {
    return this.http.get<Wallet>(`/wx/get-wallet/${openId}`);
  }
}
