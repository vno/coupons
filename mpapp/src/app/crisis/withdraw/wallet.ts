export interface Wallet {
  openid: string;
  alipay?: string;
  aliname?: string;
  totalBalance?: number;
  rate?: number;
  balance: number;
  brokerage?: number;
}
