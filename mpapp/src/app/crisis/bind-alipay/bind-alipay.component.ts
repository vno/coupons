import {Component, OnInit} from '@angular/core';
import {BindAlipayService} from './bind-alipay.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DialogService} from 'ngx-weui';
import {Authenticate} from '../../core/authenticate.service';

@Component({
  selector: 'app-build-alipay',
  templateUrl: './bind-alipay.component.html',
  styleUrls: ['./bind-alipay.component.scss']
})
export class BindAlipayComponent implements OnInit {

  bindAli: { alipay: string, aliname: string, openId: string } = {alipay: '', aliname: '', openId: ''};
  bindSuccess = false;
  loading = false;

  constructor(
    private auth: Authenticate,
    private route: ActivatedRoute,
    private router: Router,
    private srv: DialogService,
    private service: BindAlipayService) {
  }

  ngOnInit() {
    this.bindAli.openId = this.auth.authenticator.info.openId;
  }

  onSubmit() {
    this.srv.show({
      title: '确认你的支付宝',
      content: `<div class="weui-cells">
        <div class="weui-cell">
          <div class="weui-cell__hd">
            <label class="weui-label">支付宝:</label>
          </div>
          <div class="weui-cell__bd">
             <span class="text-primary">${this.bindAli.alipay}</span>
          </div>
        </div>
        <div class="weui-cell">
          <div class="weui-cell__hd">
            <label class="weui-label">真实名:</label></div>
          <div class="weui-cell__bd">
           <span class="text-primary">${this.bindAli.aliname}</span>
          </div>
        </div>
      </div>
      <div class="weui-cells__tips mt-2">
        <p class="text-danger mt-2"><i class="weui-icon-warn"></i>支付宝绑定后无法修改,请仔细核对支付宝信息后提交.</p>
      </div>`
    }).subscribe(result => {
      if (result.value) {
        this.loading = true;
        this.service.binding(this.bindAli).subscribe(() => this.bindSuccess = true, error => {
          this.srv.show({
            title: '绑定出错了',
            content: `<div class="weui-msg">
            <div class="weui-msg__text-area">
              <p class="weui-msg__desc"><i class="weui-icon-warn"></i>${error.error.message}</p>
            </div>
            </div>`, btns: [{text: '确认', type: 'primary', value: true}]
          }).subscribe(() => this.loading = false);
        });
      }
    });
  }

}
