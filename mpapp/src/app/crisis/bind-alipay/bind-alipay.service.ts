import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BindAlipayService {

  constructor(private http: HttpClient) {
  }

  binding(params: any): Observable<any> {
    return this.http.post(`/wx/bind-ali`, params);
  }

}
