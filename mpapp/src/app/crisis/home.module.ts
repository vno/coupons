import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeRoutingModule} from './home-routing.module';
import {IndexComponent} from './index/index.component';
import {SubmitOrderComponent} from './submit-order/submit-order.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {OrderComponent} from './order/order.component';
import {WeUiModule} from 'ngx-weui';
import {SharePreFeePipe} from './share-pre-fee.pipe';
import {WithdrawComponent} from './withdraw/withdraw.component';
import {BindAlipayComponent} from './bind-alipay/bind-alipay.component';
import {NgZorroAntdModule} from 'ng-zorro-antd';

@NgModule({
  declarations: [
    IndexComponent,
    SubmitOrderComponent,
    OrderComponent,
    SharePreFeePipe,
    WithdrawComponent,
    BindAlipayComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    WeUiModule.forRoot(),
    NgZorroAntdModule,

    HomeRoutingModule
  ]
})
export class HomeModule {
}
