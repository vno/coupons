import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Authenticate} from '../../core/authenticate.service';
import {Authenticator} from '../../core/authenticator';

@Injectable({
  providedIn: 'root'
})
export class IndexService {

  constructor(private http: HttpClient,
              private auth: Authenticate) {
  }

  getOrders(openId: string) {
    return this.http.get(`/wx/orders?sort=createTime,DESC&size=2&openId=${openId}`);
  }

  getAuthenticator(): Authenticator {
    return this.auth.authenticator;
  }

}
