import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DialogService} from 'ngx-weui';
import {IndexService} from './index.service';
import {Authenticator} from '../../core/authenticator';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  user: Authenticator;

  orderModel: any;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private service: IndexService,
              private srv: DialogService) {
  }

  ngOnInit() {
    this.user = this.service.getAuthenticator();
    this.searchOrder(this.user.info.openId);
  }

  searchOrder(openid: string) {
    this.service.getOrders(openid).subscribe(model => this.orderModel = model);
  }

  onShowKefu() {
    this.srv.show({
      title: '联系客服,长按加好友哦!',
      btns: [{text: '确认', type: 'primary', value: ''}],
      content: `<img src="assets/images/kefu-qrcode.jpg" width="100%" height="100%" alt="联系客服">`
    }).subscribe((res: any) => {
      console.log(res);
    });
    return false;
  }
}
