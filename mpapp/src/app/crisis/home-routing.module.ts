import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {SubmitOrderComponent} from './submit-order/submit-order.component';
import {OrderComponent} from './order/order.component';
import {WithdrawComponent} from './withdraw/withdraw.component';
import {BindAlipayComponent} from './bind-alipay/bind-alipay.component';
import {AuthGuard} from '../core/auth.guard';

const routes: Routes = [
  {
    path: 'index',
    component: IndexComponent,
    canActivate: [AuthGuard],
    data: {
      title: '个人中心'
    }
  },
  {
    path: 'sub-order',
    component: SubmitOrderComponent,
    canActivate: [AuthGuard],
    data: {
      title: '提交订单'
    }
  },
  {
    path: 'orders',
    component: OrderComponent,
    canActivate: [AuthGuard],
    data: {
      title: '订单查询'
    }
  }, {
    path: 'withdraw',
    component: WithdrawComponent,
    canActivate: [AuthGuard],
    data: {
      title: '提取余额'
    }
  }, {
    path: 'bind-ali',
    component: BindAlipayComponent,
    canActivate: [AuthGuard],
    data: {
      title: '绑定支付宝'
    }
  },
  {path: '', redirectTo: '/index', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
