import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {Order, SubmitOrderService} from './submit-order.service';
import {DialogService} from 'ngx-weui';
import {Authenticate} from '../../core/authenticate.service';

@Component({
  selector: 'app-submit-order',
  templateUrl: './submit-order.component.html',
  styleUrls: ['./submit-order.component.scss']
})
export class SubmitOrderComponent implements OnInit {

  order: Order = {tradeId: null, openId: '', type: 1};

  constructor(private service: SubmitOrderService,
              private auth: Authenticate,
              private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private srv: DialogService) {
  }

  ngOnInit() {
    this.order.openId = this.auth.authenticator.info.openId;
  }

  onSubmit() {
    this.service.submitOrder(this.order).subscribe(() =>
      this.srv.show({
        title: '提交成功', content: '你的订单已被提交,请等待系统同步!',
        btns: [{text: '返回首页', type: 'default', value: 1},
          {text: '继续', type: 'primary', value: 2}]
      }).subscribe(data => {
        if (data === 1) {
          this.onLeftClick();
        } else {
          this.order.tradeId = null;
        }
      }), () => this.srv.show({
      title: '发生错误了', content: '你的订单已被绑定或错误,请等待系统同步!',
      btns: [{text: '确定', type: 'primary', value: 'co'}]
    }).subscribe(() => this.order.tradeId = null));
  }

  onShowHelp() {

    this.srv.show({
      title: '如何获取订单号?', content: `<img src="assets/images/kefu-qrcode.jpg" width="100%" height="100%" alt="帮助">`,
      btns: [{text: '确定', type: 'primary', value: 'co'}]
    }).subscribe(() => console.log('success'));
  }

  onLeftClick() {
    this.location.back();
  }
}
