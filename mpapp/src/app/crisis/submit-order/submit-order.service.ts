import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubmitOrderService {

  constructor(private http: HttpClient) {
  }

  submitOrder(order: Order): Observable<any> {
    return this.http.post('/wx/order-commit', order);
  }

}

export interface Order {
  tradeId: number;
  openId: string;
  type: number;
}
