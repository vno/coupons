import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InstructionsComponent} from './note/instructions.component';

const routes: Routes = [
  {path: 'note', component: InstructionsComponent},
  {path: '', component: InstructionsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelpRoutingModule {
}
