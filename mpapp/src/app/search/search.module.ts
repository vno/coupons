import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {SearchRoutingModule} from './search-routing.module';
import {DetailComponent} from './detail/detail.component';
import {ClipboardModule} from 'ngx-clipboard';
import {SearchComponent} from './search.component';
import {WeUiModule} from 'ngx-weui';
import {NgZorroAntdModule} from 'ng-zorro-antd';

@NgModule({
  declarations: [
    SearchComponent,
    DetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,

    WeUiModule.forRoot(),
    NgZorroAntdModule,
    ClipboardModule,

    SearchRoutingModule
  ]
})
export class SearchModule {
}
