import {Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {SearchService} from './search.service';
import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {InfiniteLoaderComponent} from 'ngx-weui';

@Component({
  selector: 'app-super-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SearchComponent implements OnInit, OnDestroy {

  pageModels: Array<Observable<any>> = [];

  searchModel: any = {q: '', page: 1, sort: '', totalPages: 0, hasCoupon: false, isTmall: false};

  ptrLoading = 'loading';

  items: Observable<any>;

  private searchText$ = new Subject<string>();

  @ViewChild('divTarget') el: any;

  constructor(private service: SearchService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(param => {
      const q = param.get('q');
      if (q) {
        this.initData(q);
      } else {
        this.initData('');
      }
    });
  }

  initData(query: string) {
    if (query) {
      this.searchModel.q = query;
    }

    this.onLoadPageModel();

    this.items = this.searchText$.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(term => this.service.suggest(term)));
  }

  select(event: number) {
    if (1 === event) {
      this.searchModel = {
        q: this.searchModel.q,
        page: 1,
        sort: 'total_sales_des',
        totalPages: 0,
        hasCoupon: false,
        isTmall: false
      };
    } else if (2 === event) {
      this.searchModel.sort = 'price_asc';
    } else if (3 === event) {
      this.searchModel.sort = 'total_sales_des';
    } else if (4 === event) {
      this.searchModel.sort = 'tk_rate_des';
    } else if (5 === event) {
      this.searchModel.hasCoupon = !this.searchModel.hasCoupon;
    } else if (6 === event) {
      this.searchModel.isTmall = !this.searchModel.isTmall;
    } else {
      this.searchModel.sort = 'total_sales_des';
    }
    this.onLoadPageModel();
  }

  onSearch(event: any) {
    if (event && typeof (event) === 'string') {
      this.searchText$.next(event);
    }
  }

  onSuggest(value: string) {
    this.onSubmit(value);
  }

  onClear() {
    this.searchModel.q = '';
    this.onSubmit('');
  }

  onSubmit(value: string) {
    if (value) {
      this.searchModel.q = value;
    }

    this.searchText$.next();

    this.onLoadPageModel();
  }

  onLoadMore(comp: InfiniteLoaderComponent) {
    this.searchModel.page++;
    if (this.searchModel.page < this.searchModel.totalPages) {
      const o = this.service.search(this.searchModel).pipe(tap(() => comp.resolveLoading()));
      this.pageModels.push(o);
    } else {
      comp.setFinished();
    }

  }

  onLoadPageModel() {
    this.pageModels = [];
    this.ptrLoading = 'loading';
    this.pageModels.push(this.service.search(this.searchModel)
      .pipe(tap((model) => {
        this.ptrLoading = 'line';
        this.searchModel.totalPages = model.page.totalPages;
      })));
  }

  ngOnDestroy() {
    /* 取消订阅 */
    this.searchText$.next();
    this.searchText$.complete();
  }
}
