import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Authenticate} from '../core/authenticate.service';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private  http: HttpClient,
              private auth: Authenticate) {
  }

  search(searchModel: any): Observable<any> {
    searchModel.openid = this.auth.authenticator.info.openId;
    return this.http.get(`/wx/search/all`, {params: searchModel});
  }

  suggest(q: string): Observable<any> {
    return this.http.jsonp(`https://suggest.taobao.com/sug?&code=utf-8&q=${q}&qq-pf-to=pcqq.group `, 'callback');
  }


}
