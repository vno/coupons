import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DetailService {

  constructor(private http: HttpClient) {
  }

  getGoods(openId: string, id: string): Observable<any> {
    return this.http.get(`/wx/search/${openId}/${id}`);
  }

  goodsRecommend(id: string): Observable<any> {
    return this.http.get(`/wx/search/recommend/${id}`);
  }
}
