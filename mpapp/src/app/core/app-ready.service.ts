import {Injectable} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {AppLoadingService} from './app-loading.service';
import {AppWxReadyService} from './app-wx-ready.service';

@Injectable({providedIn: 'root'})
export class AppReadyService {

  private _TITLE: string;

  constructor(private titleSrc: Title,
              private loading: AppLoadingService,
              private wxSer: AppWxReadyService) {
    loading.init();
    this.wxSer.wxReady.subscribe(wx => wx.hideAllNonBaseMenuItem());
  }

  public set title(title: string) {
    this._TITLE = title;
    this.titleSrc.setTitle(this._TITLE);
  }

  public get title(): string {
    return this._TITLE;
  }

  public wxInit() {
    this.wxSer.initWx();
  }
}
