import {Wallet} from '../crisis/withdraw/wallet';

export interface Authenticator {
  info: {
    openId: string,
    headImgUrl: string,
    nickname: string
  };
  wallet: Wallet;
  shareNumber: number;
}
