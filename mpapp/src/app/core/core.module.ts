import {Inject, LOCALE_ID, NgModule, Optional, SkipSelf} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {httpInterceptorProviders} from './http-interceptors';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {AppWxReadyService} from './app-wx-ready.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    RouterModule
  ],
  declarations: [],
  providers: [
    Title,
    httpInterceptorProviders,
    AppWxReadyService
  ],
  exports: [HttpClientModule]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule,
              @Inject(LOCALE_ID) private locale: string) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
