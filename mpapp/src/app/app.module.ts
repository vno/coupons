import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import localeZh from '@angular/common/locales/zh-Hans';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule, registerLocaleData} from '@angular/common';

import {AppComponent} from './app.component';
import {HomeModule} from './crisis/home.module';
import {RouterModule, Routes} from '@angular/router';
import {CoreModule} from './core/core.module';

import {WeUiModule} from 'ngx-weui';
import {SearchModule} from './search/search.module';
import {NoneAuthComponent} from './components/none-auth/none-auth.component';
import {NgZorroAntdModule, NZ_I18N, zh_CN} from 'ng-zorro-antd';
import {HelpModule} from './help/help.module';

registerLocaleData(localeZh, 'zh-Hans');

const routes: Routes = [
  {path: 'none-auth', component: NoneAuthComponent},
  {path: '', redirectTo: '/index', pathMatch: 'full'},
  {path: '**', redirectTo: '/none-auth'}
];

@NgModule({
  declarations: [
    AppComponent,
    NoneAuthComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,

    WeUiModule.forRoot(),
    NgZorroAntdModule,

    CoreModule,
    SearchModule,
    HomeModule,
    HelpModule,

    RouterModule.forRoot(routes),
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'zh-Hans'},
    {provide: NZ_I18N, useValue: zh_CN}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
